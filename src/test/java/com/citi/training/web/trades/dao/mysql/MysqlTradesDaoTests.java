package com.citi.training.web.trades.dao.mysql;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.web.trades.dao.mysql.MysqlTradesDao;
import com.citi.training.web.trades.exceptions.TradeNotFoundException;
import com.citi.training.web.trades.model.Trade;

@RunWith(SpringRunner.class)
@ActiveProfiles("h2test")
@SpringBootTest
@Transactional
public class MysqlTradesDaoTests {
	
	@SpyBean
	JdbcTemplate tpl;

	@Autowired
	MysqlTradesDao mysqltradesDao;

	@Test
	public void test_createAndFindAll_runs() {
		mysqltradesDao.create(new Trade(76, "AAPL", 345.78, 4341));	
		assertThat(mysqltradesDao.findAll().size(), equalTo(1));
	}

    @Test
    public void test_createAndFindById_works() {
        int newId = mysqltradesDao.create(
                            new Trade(80, "AAPL", 346.87, 431));
        assertNotNull(mysqltradesDao.findById(newId));
    }

    @Test(expected=TradeNotFoundException.class)
    public void test_createAndFindById_throwsNotFound() {
        mysqltradesDao.create(new Trade(81, "AAPL", 347.87, 435));
        mysqltradesDao.findById(81);
    }
	
}
