package com.citi.training.web.trades.service;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.training.web.trades.dao.TradesDao;
import com.citi.training.web.trades.model.Trade;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TradesServiceTests {

	@Autowired
	private TradesService tradeService;
	
	@MockBean
	private TradesDao mockTradeDao;
	
	@Test
	public void test_createRuns() {
		int testId = 15;
		
		when(mockTradeDao.create(any(Trade.class))).thenReturn(testId);
		
		Trade testTrade = new Trade(1, "AAPL", 156.9, 467);	
		
		int createdId = tradeService.create(testTrade);
		
		verify(mockTradeDao).create(testTrade);
		assertEquals(testId, createdId);
	}
	
	@Test
	public void test_deleteRuns() {
		int testId = 55;	

		tradeService.deleteById(55);
		
		verify(mockTradeDao).deleteById(testId);	
	}
	
	@Test
	public void test_findAll() {
		Trade testTrade1 = new Trade(1, "AAPL", 156.9, 467);	
		Trade testTrade2 = new Trade(2, "MSFT", 170, 350);	
		
		List<Trade> TradeList = new ArrayList();
		
		TradeList.add(testTrade1);
		TradeList.add(testTrade2);
		
		when(mockTradeDao.findAll()).thenReturn(TradeList);
			
		List<Trade> resultList = tradeService.findAll()	;
		
		assertEquals(resultList,TradeList);
		}
	
	@Test
	public void test_findById() {
		int testId = 55;
		
		Trade testTrade = new Trade(55, "AAPL", 130.0, 555);
		
		when(mockTradeDao.findById(testId)).thenReturn(testTrade);
		
		Trade returnedTrade = tradeService.findById(testId);
		
		verify(mockTradeDao).findById(testId);
		assertEquals(testTrade,returnedTrade);
	}

}
