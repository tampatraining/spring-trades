package com.citi.training.web.trades.model;

import static org.junit.Assert.*;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.citi.training.web.trades.model.*;

public class TradesTest {

	private static final Logger logger = LoggerFactory.getLogger(TradesTest.class);

	@Test
	public void test_Trade_fullConstructor() {
		Trade testTrade = new Trade(0, "AAPL", 156.9, 467);

		assert (testTrade.getId() == 0);
		assert (testTrade.getStock().equals("AAPL"));
		assert (testTrade.getPrice() == 156.9);
		assert (testTrade.getVolume() == 467);
	}

	@Test
	public void test_Trade_setters() {
		Trade testTrade = new Trade(0, "AAPL", 156.9, 467);

		testTrade.setId(34);
		testTrade.setStock("MSFT");
		testTrade.setPrice(26.43);
		testTrade.setVolume(512);

		assert (testTrade.getId() == 34);
		assert (testTrade.getStock().equals("MSFT"));
		assert (testTrade.getPrice() == 26.43);
		assert (testTrade.getVolume() == 512);
	}

	@Test
	public void test_Trade_toString() {
		Trade testTrade = new Trade(0, "AAPL", 156.9, 467);

		assert (testTrade.toString().contains(testTrade.getStock()));

		
		assert (testTrade.toString().contains(
				Integer.toString(testTrade.getId())));
		
		assert (testTrade.toString().contains(
				Double.toString(testTrade.getPrice())));
		
		assert (testTrade.toString().contains(
				Integer.toString(testTrade.getVolume())));
	}

}
