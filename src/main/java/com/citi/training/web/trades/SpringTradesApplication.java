package com.citi.training.web.trades;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringTradesApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringTradesApplication.class, args);
	}

}
