package com.citi.training.web.trades.dao;

import java.util.List;

import com.citi.training.web.trades.model.Trade;
/**
 * 
 * @author Administrator
 * <p>Interface for Trades Database</p>
 */
public interface TradesDao {
	
	List<Trade> findAll();
	int create(Trade Trade);
	Trade findById(int id);
	void deleteById(int id);
	
}
