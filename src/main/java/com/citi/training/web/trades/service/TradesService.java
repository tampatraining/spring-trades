package com.citi.training.web.trades.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.citi.training.web.trades.dao.TradesDao;
import com.citi.training.web.trades.model.Trade;

/**
 * <p>Service layer for Trades</p>
 * @author Administrator
 *
 */

@Component
public class TradesService {

	@Autowired
	TradesDao tradesDao;
	
	public List<Trade> findAll(){
		return tradesDao.findAll();
	}
		
	public int create(Trade Trade) {
		 return tradesDao.create(Trade);
	 }
	public Trade findById(int id) {
	    	return tradesDao.findById(id);
	 }
	 public void deleteById(int id) {
		 tradesDao.deleteById(id);
	 }
}
