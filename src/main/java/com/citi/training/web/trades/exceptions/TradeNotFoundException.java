package com.citi.training.web.trades.exceptions;
/**
 * 
 * @author Administrator
 * <p>Exception for not finding a trade by id in database</p>
 */
@SuppressWarnings("serial")
public class TradeNotFoundException extends RuntimeException {
	 public TradeNotFoundException(String msg) {
	        super(msg);
	    }
}
