package com.citi.training.web.trades.dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import com.citi.training.web.trades.dao.TradesDao;
import com.citi.training.web.trades.exceptions.TradeNotFoundException;
import com.citi.training.web.trades.model.Trade;
/**
 * 
 * @author Administrator
 * <p>Trades Dao implementation</p>
 */
@Component
public class MysqlTradesDao implements TradesDao {

	Logger logger = LoggerFactory.getLogger(MysqlTradesDao.class);
	
	@Autowired
	JdbcTemplate tpl;

	public List<Trade> findAll() {
		logger.debug("Selecting trade from database");
		
		return tpl.query("select id, stock, price, volume from trade", new TradeMapper());
	}

	@Override
	public Trade findById(int id) {
		logger.debug("Finding trade from database");
		
		List<Trade> Trades = this.tpl.query("select id, stock, price,"
				+ " volume from trade where id = ?", new Object[] { id },
				new TradeMapper());

		if(Trades.size() <= 0) {
	            throw new TradeNotFoundException("Error, no trade found");
	     }
		
		return Trades.get(0);
	}

	@Override
	public int create(Trade Trade) {
		logger.debug("Creating trade in database");
		
		KeyHolder keyHolder = new GeneratedKeyHolder();
		this.tpl.update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection connection)throws SQLException {
				PreparedStatement ps = connection.prepareStatement
						("insert into trade (stock, price,volume) values (?, ?, ?)",
						Statement.RETURN_GENERATED_KEYS);
				
				ps.setString(1, Trade.getStock());
				ps.setDouble(2, Trade.getPrice());
				ps.setInt(3, Trade.getVolume());
				
				return ps;
			}
		}, keyHolder);
		return keyHolder.getKey().intValue();
	}

	@Override
	public void deleteById(int id) {
		logger.debug("Deleting trade in database");
		
		int deleteResult = this.tpl.update("delete from trade where id=?", id);
		
		if(deleteResult == 0) {
			throw new TradeNotFoundException("Error, trade not found");
		}
	}

	private static final class TradeMapper implements RowMapper<Trade> {
		public Trade mapRow(ResultSet rs, int rowNum) throws SQLException {
			
			return new Trade(rs.getInt("id"), rs.getString("stock")
					, rs.getDouble("price"), rs.getInt("volume"));
		}
	}
}
